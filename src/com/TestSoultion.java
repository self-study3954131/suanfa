package com;

import com.lianxi.*;

import java.util.Arrays;

public class TestSoultion {
    public static void main(String[] args) {
        //爬楼梯
        System.out.println("==============爬楼梯============");
        System.out.println(ClimbingStairs.climbStairsWithRecursive(7));
        System.out.println(ClimbingStairs.climbStairs(5));
        System.out.println(ClimbingStairs.climbStairs2(6));
        System.out.println("===============================");

        //斐波那契数列
        System.out.println("==========斐波那契数列===========");
        System.out.println(ClimbingStairs2.climbStairsWithRecursive(4));
        System.out.println(ClimbingStairs2.climbStairs(5));
        System.out.println(ClimbingStairs2.climbStairs2(6));
        System.out.println("================================");

        //两数之和
        System.out.println("==============两数之和===========");
        int[] a = {2, 7, 11, 15};
        System.out.println(Arrays.toString(TowSum.towSum(a, 9)));
        System.out.println(Arrays.toString(TowSum.fastWithMap2(a, 9)));
        System.out.println("================================");

        //合并两个有序数组
        System.out.println("==========合并两个有序数组=========");
        int[] nums1 = {1, 2, 3, 0, 0, 0};
        int m = 3;
        int[] nums2 = {2, 5, 6};
        int n = 3;
        System.out.println(Arrays.toString(Merge.merge(nums1, m, nums2, n)));
        System.out.println("================================");

        //移动零
        System.out.println("==============移动零=============");
        int[] nums = {6, 5, 0, 1, 11, 0, 3, 12};
        System.out.println(Arrays.toString(MoveZeroes.moveZeroes(nums)));
        System.out.println("================================");
    }
}
