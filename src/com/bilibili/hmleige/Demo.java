package com.bilibili.hmleige;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;

/**
 * @Author : hugaoqiang
 * @CreateTime : 2023-06-02  09:54
 */
public class Demo {
    public static void main(String[] args) throws IOException {
        /*
         * 求1 -> n的乘阶
         * 问题：1*2*3*4*5*6....n*n
         * 公式:
         *      f(n) = f(n - 1) * n
         * 终结点:
         *      f(1) = 1;
         */
        System.out.println("5的乘阶是: " + f(5));

        /*
         * 求1 -> n的和
         * 问题：1+2+3+4+5+6....n+n
         * 公式:
         *      f(n) = f(n - 1) + n
         * 终结点:
         *      f(1) = 1;
         */
        System.out.println("1 -> 5的和是: " + h(5));

        /*
         * 猴子吃桃
         * 公式：
         *      d(n) - d(n) / 2 - 1 = d(n + 1)
         * 变形：
         *      2d(n) - d(n) - 2 = 2d(n + 1)
         *      2d(n) - d(n) = 2d(n + 1) + 2
         *      d(n) = 2d(n + 1) + 2
         * 终结点:
         *      d(10) = 1;
         */
        System.out.println("第一天摘了:" + d(1) + "个桃");
        System.out.println("第二天摘了:" + d(2) + "个桃");
        System.out.println("第三天摘了:" + d(3) + "个桃");
        System.out.println("第四天摘了:" + d(4) + "个桃");
        System.out.println("第五天摘了:" + d(5) + "个桃");
        System.out.println("第六天摘了:" + d(6) + "个桃");
        System.out.println("第七天摘了:" + d(7) + "个桃");
        System.out.println("第八天摘了:" + d(8) + "个桃");
        System.out.println("第九天摘了:" + d(9) + "个桃");

        /*
         * 文件搜索的实现
         */
        searchFile(new File("/Users/hugaoqiang/Desktop/code/"), "1685080868670.png");

        /*
         * 删除文件夹
         */
        deleteFile(new File(""));

        /*
         * copy文件夹
         */
        copyDirectory(new File("/Users/hugaoqiang/Desktop/pam/"), new File("/Users/hugaoqiang/Desktop/pam2/"));

        /*
         * 查询某个文件夹下所有文件
         */
        findAllFile(Paths.get("/Applications/"));

        /*
         * n / 2 = 5 可以买的啤酒数量
         *      5瓶啤酒有5个盖子和5个空瓶  可以换3瓶啤酒  剩1个盖子1个空瓶
         *
         * 4个盖子 = 1瓶啤酒  x
         * 2个空瓶 = 1瓶啤酒  y
         */
        int money = 10;
        int bottles = 0;
        int caps = 0;
        int beers = drinkBeer(money, bottles, caps);
        System.out.println("可以喝 " + beers + " 瓶酒");
        System.out.println("还剩 " + bottles + " 个空瓶和 " + caps + " 个盖子");
    }

    private static int f(int n) {
        if (n == 1) return 1;
        return f(n - 1) * n;
    }

    private static int h(int n) {
        if (n == 1) return 1;
        return f(n - 1) + n;
    }

    private static int d(int n) {
        if (n == 10) return 1;
        return d(n + 1) * 2 + 2;
    }

    private static void searchFile(File dir, String fileName) {
        //把非法的情况都拦截掉
        if (dir == null || !dir.exists() || dir.isFile()) return;
        //获取文件夹下的所有目录
        File[] files = dir.listFiles();
        //排除非法情况并遍历files
        if (files != null || files.length > 0) for (File file : files) {
            if (file.isFile()) {//如果是文件
                if (file.getName().contains(fileName)) {//判断是否是我需要找的文件
                    System.out.println("找到了：" + file.getAbsolutePath());
                }
            } else {//是文件夹 重复上述过程 递归调用
                searchFile(file, fileName);
            }
        }
    }

    private static void deleteFile(File dir) {
        //卫语句拦截极端情况拦截
        if (dir == null || !dir.exists()) return;
        //如果是文件直接删除返回
        if (dir.isFile()) {
            dir.delete();
            return;
        }
        //获取文件夹下的所有目录
        File[] files = dir.listFiles();
        //排除非法情况并遍历files
        if (files != null || files.length > 0) for (File file : files) {
            if (file.isFile()) {//是文件直接删除
                file.delete();
            } else {//是文件夹 重复上述过程 递归调用
                deleteFile(file);
            }
        }
        else {//没有文件代表是空的，直接删除
            dir.delete();
        }
    }

    private static void copyDirectory(File src, File dest) {
        //卫语句拦截极端情况拦截
        if (src == null || dest == null || !src.exists() || !dest.exists() || src.isFile() || dest.isFile()) return;
        //创建文件夹
        File destNew = new File(dest, src.getName());
        destNew.mkdir();
        //获取所有一级文件/文件夹
        File[] files = src.listFiles();
        //排除非法情况并遍历files
        if (files != null && files.length > 0) for (File file : files) {
            if (file.isFile()) {//是文件 直接copy
                copyFileToFile(file, new File(destNew, file.getName()));
            } else {//是文件夹 重复上述过程 递归调用
                copyFileToFile(file, destNew);
            }
        }
    }

    private static void copyFileToFile(File srcNew, File destNew) {
        try {
            //jdk1.8自带的一行拷贝文件  srcNew.getAbsolutePath()//获取文件绝对路径
            Files.copy(Paths.get(srcNew.getAbsolutePath()), Paths.get(destNew.getAbsolutePath()));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private static int drinkBeer(int money, int bottles, int caps) {
        int beers = money / 2;// 可以买的酒瓶数
        bottles += beers; // 新增的空瓶数量
        caps += beers; // 新增的盖子数量
        if (bottles < 2 && caps < 4) {
            return beers; // 没有空瓶也没有盖子可以兑换，直接返回
        }
        int newbeers = 0;
        if (bottles >= 2) {
            int newBottles = bottles / 2; // 可以兑换的酒瓶数
            bottles = bottles % 2 + newBottles; // 剩余的空瓶数量
            newbeers += newBottles;
        }
        if (caps >= 4) {
            int newCaps = caps / 4; // 可以兑换的酒瓶数
            caps = caps % 4 + newCaps; // 剩余的盖子数量
            newbeers += newCaps;
        }
        return beers + drinkBeer(newbeers * 2, bottles, caps); // 递归调用
    }

    private static void findAllFile(Path path) throws IOException {
        Files.walkFileTree(path, new SimpleFileVisitor<Path>() {

            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) {
                System.out.println(file);
                return FileVisitResult.CONTINUE;
            }

            @Override
            public FileVisitResult visitFileFailed(Path file, IOException exc) {
                System.err.println(exc);
                return FileVisitResult.CONTINUE;
            }
        });
    }

}
