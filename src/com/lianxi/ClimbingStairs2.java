package com.lianxi;

import java.util.HashMap;
import java.util.Map;

/**
 * 斐波那契数列
 * 写一个函数，输入n，求斐波那契数列的第n项，斐波那契数列的定义如下：
 * {0              n = 0
 * f(n){1          n = 1
 * {f(n-2)_f(n-1)  n > 1
 */
public class ClimbingStairs2 {

    private static final Map<Integer, Integer> storeMap = new HashMap<>();

    //递归的解法
    public static int climbStairsWithRecursive(int n) {
        if (n == 0) return 0;
        if (n == 1) return 1;
        return climbStairsWithRecursive(n - 1) + climbStairsWithRecursive(n - 2);
    }

    //递归的解法，用HashMap存储中间计算结果
    public static int climbStairs(int n) {
        if (n == 0) return 0;
        if (n == 1) return 1;
        if (null != storeMap.get(n))
            return storeMap.get(n);
        else {
            int result = climbStairs(n - 2) + climbStairs(n - 1);
            storeMap.put(n, result);
            return result;
        }
    }

    //循环的解法，自底向上累加
    public static int climbStairs2(int n) {
        if (n == 0) return 0;
        if (n == 1) return 0;
        int result = 0;
        int pre = 1;
        int prePre = 0;
        for (int i = 2; i <= n; i++) {
            result = pre + prePre;
            prePre = pre;
            pre = result;
        }
        return result;
    }

}
