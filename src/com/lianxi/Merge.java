package com.lianxi;

/**
 * 合并两个有序数组
 * 给定两个按非递减顺序排列的整数数组nums1和nums2，另有两个整数m和n，
 * 分别表示nums1和nums2中的元素数目。
 * 请你合并nums2到nums1中，使合并后的数组同样按非递减顺序排列。
 * 注意：最终，合并后数组不应有函数返回，而是存储在数组nums1中，为了应对这
 * 种情况，nums1的初始长度为n+m，其中前m个元素比哦啊是应合并的元素，后n
 * 个元素为0，应忽略，nums2的长度为n。
 */
public class Merge {

    public static int[] merge(int[] nums1, int m, int[] nums2, int n) {
        int k = m + n;
        int[] temp = new int[k];
        for (int index = 0, nums1Index = 0, nums2Index = 0; index < k; index++) {
            if (nums1Index >= m) {//nums1数组已经取完，完全取nums2数组的值即可
                temp[index] = nums2[nums2Index++];
            } else if (nums2Index >= n) {//nums2数组已经取完，完全取nums1数组的值即可
                temp[index] = nums1[nums1Index++];
            } else if (nums1[nums1Index] < nums2[nums2Index]) {//nums1的元素值大于nums2，取nums1值
                temp[index] = nums1[nums1Index++];
            } else {//nums2的元素值大于nums1，取nums2值
                temp[index] = nums2[nums2Index++];
            }
        }
        //复制数组
        System.arraycopy(temp, 0, nums1, 0, k);
        return nums1;
    }

 /*   public static int[] merge(int[] nums1, int m, int[] nums2, int n){
        int k = m+n;
        for(int index = k-1,nums1Index = m-1,nums2Index = n-1; index >= 0; index--){
            if (nums1Index < 0){*//*nums1数组已经取完，完全取nums2数组的值即可*//*
                nums1[index] = nums2[nums2Index--];
            }else if (nums2Index < 0){*//*nums2数组已经取完，完全取nums1数组的值即可*//*
                break;
            } else if (nums1[nums1Index] > nums2[nums2Index]){*//*nums1的元素值大于nums2，取nums1值*//*
                nums1[index] = nums1[nums1Index--];
            }else {*//*nums2的元素值大于nums1，取nums2值*//*
                nums1[index] = nums2[nums2Index--];
            }
        }
        return nums1;
    }*/

}
