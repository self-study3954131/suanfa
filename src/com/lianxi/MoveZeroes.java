package com.lianxi;

import java.util.Arrays;

/**
 * 移动零
 * 给定一个数组nums，编写一个函数将所有0移动到数组的末尾，同事保持非零元素的相对顺序。
 */
public class MoveZeroes {

    public static int[] moveZeroes(int[] nums) {
        if (nums == null) {
            return null;
        }
        //排序
        Arrays.sort(nums);
        //第一次遍历的时候，j指针记录非0的个数，只要是非0的统统都赋给nums[j]
        int j = 0;
        for (int i = 0; i < nums.length; ++i) {
            if (nums[i] != 0) {
                nums[j++] = nums[i];
            }
        }
        //非0元素统计完了，剩下的都是0了
        //所以第二次遍历把末尾的元素都赋为0即可
        for (int i = j; i < nums.length; ++i) {
            nums[i] = 0;
        }
        return nums;
    }

}
