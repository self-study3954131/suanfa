package com.lianxi;

import java.util.HashMap;
import java.util.Map;

/**
 * 两数之和
 * 给定一个整数组nums和一个整数目标值target，请你在给数组中找出和为目标值target的那两
 * 个整数，并返回他们的数组下标。
 * 你可以假设每种输入只会对应一个答案，但是，数组中同一个元素在答案李不能重复出现。
 * 你可以按任意循序返回答案。
 */
public class TowSum {

//    public static int[] towSum(int[] nums, int target){
//        int[] result = new int[2];
//        for (int i = 0; i < nums.length; i++) {
//            for (int j = i+1; j < nums.length; j++) {
//                if (nums[i]+nums[j] == target){
//                    result[0] = i;
//                    result[1] = j;
//                    return result;
//                }
//            }
//        }
//        return result;
//    }

    /*用一个哈希表，存储每个数对应的下标，时间复杂度是O(n)*/
    public static int[] towSum(int[] nums, int target) {
        /*key为元素值，values为每个元素对应的下标*/
        Map<Integer, Integer> storeNums = new HashMap<>(nums.length, 1);
        //定义一个长度为2的int类型数组，用于存储两个整数的下标
        int[] result = new int[2];
        for (int i = 0; i < nums.length; i++) {
            //用目标值减去当前下标值
            int another = target - nums[i];
            //通过another去HashMap中寻找该值
            Integer anotherIndex = storeNums.get(another);
            //判断anotherIndex是否为空
            if (anotherIndex != null) {//不为空
                //将anotherIndex下标放到定义好的int数组第一位
                result[0] = anotherIndex;
                //将i下标放到定义好的int数组第二位
                result[1] = i;
                //结束循环
                break;
            } else {//为空
                //将i下标存入HashMap
                storeNums.put(nums[i], i);
            }
        }
        //返回结果
        return result;
    }

    /*用一个哈希表，存储每个数对应的下标，时间复杂度是O(n)*/
    public static int[] fastWithMap2(int[] nums, int target) {
        Map<Integer, Integer> map = new HashMap<>(nums.length, 1);
        int[] result = new int[2];
        for (int i = 0; i < nums.length; i++) {
            if (map.containsKey(nums[i])) {
                Integer anotherIndex = map.get(nums[i]);
                result[0] = anotherIndex;
                result[1] = i;
                break;
            } else {
                map.put(target - nums[i], i);
            }
        }
        return result;
    }
}