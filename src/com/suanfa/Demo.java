package com.suanfa;

/**
 * 时间复杂度分析
 * 只关注循环执行次数最多的一段代码
 * 常见时间复杂度
 * O(1)              常数阶
 * O(n)              线性阶
 * O(n2)             平方阶
 * O(logn)           对数阶
 * O(nlogn)          线性对数阶
 * O(n3)             立方阶
 * O(2n)             指数阶
 * O(n!)             阶乘阶
 * 从小到大一次是：
 * O(1)<O(logn)<O(n)<O(nlogn)<O(n2)<O(n3)<O(2n)<(n!)<(nn)
 */
public class Demo {

    /*
    总复杂度等于最高阶项复杂度
    总复杂度：O(n2+n+100)=最高阶复杂度：O(n2)
     */
    int cal3(int n) {
        int sum_1 = 0;
        for (int p = 1; p < 100; p++) {   //O(100)
            sum_1 = sum_1 + p;
        }

        int sum_2 = 0;
        for (int q = 1; q < n; q++) {     //O(n)
            sum_2 = sum_2 + n;
        }

        int sum_3 = 0;
        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= n; j++) {  //O(n2)
                sum_3 = sum_3 + i * j;
            }
        }
        return sum_1 + sum_2 + sum_3;
    }

    /*
    嵌套代码的复杂度等于嵌套内外代码复杂度的乘积
    内外代码复杂度：O(n)  O(n)
    嵌套代码的复杂度：O(n)*O(n)=O(n2)
     */
    int complexCal(int n) {
        int ret = 0;
        for (int i = 0; i < n; i++) {  //O(n)   外
            ret = ret + f(i);          //⬇️
        }
        return ret;
    }

    //*  = O(n2)
    int f(int n) {
        int sum = 0;
        int i = 1;                    // ⬆️
        for (; i < n; ++i) {           //O(n)    内
            sum = sum + 1;
        }
        return sum;
    }

    int mnCal(int m, int n) {
        int sum_1 = 0;
        int i = 1;
        for (; i < m; ++i) {
            sum_1 = sum_1 + i;
        }

        int sum_2 = 0;
        int j = 1;
        for (; j < n; ++j) {
            sum_2 = sum_2 + j;
        }
        return sum_1 + sum_2;
    }

}
